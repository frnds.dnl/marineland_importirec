﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Configuration;
using Import_Excel.Models;
using System.Security.Cryptography;
using System.Globalization;

namespace Import_Excel
{
    internal static class CtrlFile
    {
        private static ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();


        public static int chargeFichier(string _filePath)
        {
            try
            {
                CtrlLog.DemarreLog();
                CtrlLog.EcritLog("Vérification du fichier. " + _filePath);
               
                string filetmp = Path.GetDirectoryName(_filePath) + "\\" + Path.GetFileNameWithoutExtension(_filePath) + ".csv";

                int result = 0;
                string decrypted;

                
                byte[] cipherText = File.ReadAllBytes(_filePath);
                byte[] key = Encoding.Default.GetBytes("1Z3d6S7e9J1h3T5u");

                if (cipherText == null || cipherText.Length <= 0)
                {
                    throw new ArgumentNullException("cipherText");
                }

                if (key == null || key.Length <= 0)
                {
                    throw new ArgumentNullException("key");
                }

                using (var rijAlg = new RijndaelManaged())
                {
                    rijAlg.KeySize = 128;
                    rijAlg.BlockSize = 128;
                    rijAlg.Key = key;
                    rijAlg.Mode = CipherMode.ECB;
                    rijAlg.Padding = PaddingMode.PKCS7;
                    rijAlg.IV = key;
                    ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                decrypted = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }

                if (decrypted != null)
                    result = readFile(decrypted);
                else
                    throw new Exception("Le décryptage du fichier a échoué.");


                //////////////////////////////////////////////////////////////////////////////////////////////////////
                /********************** ANCIENNE VERSION AVEC TRAITEMENT DE FICHIER EXCEL    ************************/
                //////////////////////////////////////////////////////////////////////////////////////////////////////

                //if (File.Exists(filetmp)) // efface le fichier tmp s'il existe déjà
                //    File.Delete(filetmp);

                //Process ConvertXLS = new Process();
                //ConvertXLS.StartInfo.FileName = "ConvertXLSX.exe";
                //ConvertXLS.StartInfo.WorkingDirectory = ConfigurationManager.AppSettings["DossierAppli"] + "\\ConvertXLSX\\"; // chemin du fichier d'export
                //ConvertXLS.StartInfo.Arguments = "\"" + _filePath; // arguments : fichier de départ - fichier d'arrivée
                //ConvertXLS.Start();
                //ConvertXLS.WaitForExit();
                //exitcode = ConvertXLS.ExitCode;

                //if (exitcode == 0)
                //{
                //    // traite le fichier tmp puis l'efface
                //    result = readFile(_filePath);
                //    string csvfile = "";
                //    if (_filePath.Contains(".xls"))
                //        csvfile = _filePath.Replace(".xls", ".csv");
                //    if (_filePath.Contains(".xlsx"))
                //        csvfile = _filePath.Replace(".xlsx", ".csv");
                //    File.Delete(csvfile); // efface une fois traité
                //}


                /*********************************************************************************************/

                return result;
            }
            catch (Exception e)
            {
                CtrlLog.EcritLog("Erreur lors de la lecture du fichier. Message: " + e.Message);
                return -1;
            }
        }


        private static int readFile(string _tmpf_tmpdata)
        {
            try
            {
                //Purge des lignes précédentes
                ListEcritures.ecritures.Clear();
               

                string[] stringSeparators = new string[] { "\n" };
                string[] lines = _tmpf_tmpdata.Split(stringSeparators, StringSplitOptions.None);
                bool fichierKO = false;
                int lignenumero = 0;
                string fileErrMsg = "";
                foreach (string uneligne in lines.ToList())
                {
                    try
                    {
                        if (uneligne != "")
                        {
                            if (lignenumero > 0)
                            {

                                string ligneErrMsg = "";
                                string[] s = uneligne.Split(';');
                                //if (s.Length != 4)
                                //    throw new Exception("Fichier non valide, le nombre de champs n'est pas celui attendu. Numéro de ligne: " + lignenumero);
                                bool isOK = true;
                                Ecriture ecriture = new Ecriture();
                                string dateresult = "";
                                try
                                {
                                    dateresult = DateTime.ParseExact(s[0].Trim(), "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                                }
                                catch
                                {
                                    dateresult = DateTime.ParseExact(s[0].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                                }
                                ecriture.Date = Convert.ToDateTime(dateresult);
                                ecriture.Piece = s[1].Trim();
                                ecriture.Cg_num = s[2].Trim();
                                ecriture.CompteTiers = s[3].Trim();
                                ecriture.Libelle = s[4].Trim();

                                ecriture.Debit = Math.Round(Convert.ToDouble(s[5]), 2);
                                ecriture.Credit = Math.Round(Convert.ToDouble(s[6]), 2);

                                ecriture.Departement = s[7].Trim();
                                ecriture.Centrecout = s[8].Trim();
                                ecriture.Objectif = s[9].Trim();
                                ecriture.CodeTaxe = s[10].Trim();
                                ecriture.MontantHT = Math.Round(Convert.ToDouble(s[11]), 2);
                                ecriture.Montanttva = Math.Round(Convert.ToDouble(s[12]), 2);
                                ecriture.Tauxtva = Math.Round(Convert.ToDouble(s[13]), 2);
                                if (!string.IsNullOrEmpty(s[14]))
                                {
                                    string dateEchtxt = DateTime.ParseExact(s[14].Trim(), "yyyyMMdd", CultureInfo.InstalledUICulture).ToString("yyyy-MM-dd");
                                    ecriture.DateEcheance = Convert.ToDateTime(dateEchtxt);
                                }

                                if (ecriture.MontantHT < 0)
                                    ecriture.MontantHT = ecriture.MontantHT * (-1);
                                if (ecriture.Montanttva < 0)
                                    ecriture.Montanttva = ecriture.Montanttva * (-1);

                                if (!string.IsNullOrEmpty(ecriture.CodeTaxe))
                                    ecriture.CodeTaxeSage = ListEcritures.codesTaxes.Where(x => x.CodeFichier == ecriture.CodeTaxe).First().CodeSage;

                                // vérification de la ligne
                                if (!string.IsNullOrEmpty(ecriture.CodeTaxe) && Math.Round(ecriture.Debit + ecriture.Credit, 2) != Math.Round(ecriture.Montanttva + ecriture.MontantHT, 2))
                                {
                                    isOK = false;
                                    double ttc = Math.Round(ecriture.Debit + ecriture.Credit, 2);
                                    double tvaETHT = Math.Round(ecriture.Montanttva + ecriture.MontantHT, 2);
                                    ligneErrMsg += $"La ligne {(lignenumero +1).ToString()} a un montant code comptable {ecriture.Cg_num} avec un montant {ttc} TTC différent du montant {tvaETHT} HT et TVA." + Environment.NewLine;
                                }

                                if (ecriture.CodeTaxe == "EXO" && ecriture.Montanttva != 0)
                                {
                                    isOK = false;
                                    ligneErrMsg += "La ligne a un montant de TVA alors que le code est EXO." + Environment.NewLine;
                                }

                                //if ((ecriture.Cg_num.StartsWith("6") || ecriture.Cg_num.StartsWith("7")) && !string.IsNullOrEmpty(ecriture.CodeTaxe))
                                //{
                                //    if (Math.Round(ecriture.MontantHT + ecriture.Montanttva, 2) != Math.Round(ecriture.Debit + ecriture.Credit, 2))
                                //    {
                                //        isOK = false;
                                //        ligneErrMsg += "La ligne est de type code comptable 6 ou 7 mais le montant ht + tva est différent du montant ttc." + Environment.NewLine;
                                //    }
                                //}

                                //if ((ecriture.Cg_num.StartsWith("4") || ecriture.Cg_num.StartsWith("5")) && ecriture.Montanttva != 0)
                                //{
                                //    isOK = false;
                                //    ligneErrMsg += "La ligne est de type code comptable 4 ou 5 mais possède un montant de tva." + Environment.NewLine;
                                //}

                                if (!string.IsNullOrEmpty(ecriture.CodeTaxe) && string.IsNullOrEmpty(ecriture.CodeTaxeSage))
                                {
                                    isOK = false;
                                    ligneErrMsg += $"La code comptable {ecriture.CodeTaxe} de la ligne ne fait pas partie de la liste des codes comptables configurés." + Environment.NewLine;
                                }

                                // vérifie la présence d'un numéro de pièce et qu'il n'est pas trop long
                                //if (string.IsNullOrEmpty(ecriture.Piece))
                                //{
                                //    isOK = false;
                                //    ligneErrMsg += "Le numéro de pièce pour la ligne n'est pas renseigné." + Environment.NewLine;
                                //}
                                if (ecriture.Piece.Length > 13)
                                {
                                    isOK = false;
                                    ligneErrMsg += "Le numéro de pièce pour la ligne n'a pas la longueur adéquate (il doit être inférieur ou égal à 13 caractères)." + Environment.NewLine;
                                }
                                if (isOK)
                                {
                                    Journal journal = new Journal(ConfigurationManager.AppSettings["Journal"], ecriture.Date, new DossierCompta(CtrlOM.DossierCompta, CtrlOM.baseCompta));
                                    if (!JournalDansListeJournaux(journal, ListEcritures.listeJournaux))
                                        ListEcritures.listeJournaux.Add(journal);
                                    ListEcritures.ecritures.Add(ecriture);
                                }
                                else
                                {
                                    fichierKO = true;
                                    fileErrMsg += ligneErrMsg + Environment.NewLine;
                                    CtrlLog.EcritLog("La ligne " + (lignenumero + 1).ToString() + " n'est pas correctement renseignée: " + ligneErrMsg);
                                }
                            }
                            lignenumero++;
                        }
                    }
                    catch (Exception eLigne)
                    {
                        lignenumero++;
                        var st = new StackTrace(eLigne, true);
                        var frame = st.GetFrame(st.FrameCount - 1);
                        CtrlLog.EcritLog("Erreur lors de la lecture de la ligne " + lignenumero + ". Message: " + eLigne.Message + ". Ligne d'erreur: " + frame.GetFileLineNumber().ToString());
                        CtrlLog.EcritLogFichier("Erreur lors de la lecture de la ligne " + lignenumero + ". Message: " + eLigne.Message + ". Ligne d'erreur: " + frame.GetFileLineNumber().ToString());
                        fichierKO = true;
                    }
                }
                if (fichierKO)
                {
                    CtrlLog.EcritLogFichier(fileErrMsg);
                    return -1;                    
                }
                else
                    return 0;
            }

            catch (Exception eFile)
            {
                throw eFile;
            }
        }

        public static void recupCodeTaxe()
        {
            try
            {
                string fichierCodes = ConfigurationManager.AppSettings["DossierAppli"] + "\\codes taxes.txt";
                using (var fs = new FileStream(fichierCodes, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    StreamReader sr = new StreamReader(fs);
                    string allFile = sr.ReadToEnd();
                    string[] stringSeparators = new string[] { "\r\n" };
                    string[] lines = allFile.Split(stringSeparators, StringSplitOptions.None);
                    for (int i = 0; i < lines.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(lines[i]))
                        {
                            string[] s = lines[i].Split(';');
                            CodeTaxe ct = new CodeTaxe();
                            ct.CodeSage = s[0].ToString().Trim();
                            ct.CodeFichier = s[1].ToString().Trim();
                            ct.Designation = s[2].ToString().Trim();
                            ListEcritures.codesTaxes.Add(ct);
                        }
                    }
                }
            }
            catch (Exception eCodes)
            {
                throw new Exception("La récupération des correspondances des codes taxes a échoué. Message: " + eCodes.Message);
            }
        }

        /// <summary>
        /// vérifie si la liste des journaux contient l'élément
        /// </summary>
        /// <param name="journalCible"></param>
        /// <param name="listeJournaux"></param>
        /// <returns></returns>
        private static bool JournalDansListeJournaux(Journal journalCible, List<Journal> listeJournaux)
        {
            for (int index = 0; index < listeJournaux.Count; ++index)
            {
                Journal journal = listeJournaux[index];
                if (journalCible.dossierComptable._baseCompta == journal.dossierComptable._baseCompta && journalCible.codeJournal == journal.codeJournal && journalCible.période.CompareTo(journal.période) == 0)
                    return true;
            }
            return false;
        }

    }
}
