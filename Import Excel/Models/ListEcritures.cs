﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Import_Excel.Models
{
    public class ListEcritures
    {
        public static ObservableCollection<Ecriture> ecritures = new ObservableCollection<Ecriture>();
        public static ObservableCollection<CodeTaxe> codesTaxes = new ObservableCollection<CodeTaxe>();
        public static List<Journal> listeJournaux = new List<Journal>();
    }
}
