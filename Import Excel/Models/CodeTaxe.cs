﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_Excel.Models
{
    public class CodeTaxe
    {
        private string codeFichier;
        private string codeSage;
        private string designation;

        public string CodeFichier { get => codeFichier; set => codeFichier = value; }
        public string CodeSage { get => codeSage; set => codeSage = value; }
        public string Designation { get => designation; set => designation = value; }
    }
}
