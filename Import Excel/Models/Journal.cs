﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objets100cLib;

namespace Import_Excel.Models
{
    public class Journal
    {
        private string CodeJournal;
        private DateTime Période;
        private DossierCompta DossierComptable;

        public Journal(string pCodeJournal, DateTime pPériode, DossierCompta pDossierComptable)
        {
            this.CodeJournal = pCodeJournal;
            this.Période = pPériode;
            DossierComptable = pDossierComptable;
        }

        public string codeJournal
        {
            get
            {
                return this.CodeJournal;
            }
            set
            {
                this.CodeJournal = value;
            }
        }

        public DossierCompta dossierComptable
        {
            get
            {
                return this.DossierComptable;
            }
            set
            {
                this.DossierComptable = value;
            }
        }

        public DateTime période
        {
            get
            {
                return this.Période;
            }
            set
            {
                this.Période = value;
            }
        }
    }

    public class DossierCompta
    {
        private BSCPTAApplication100c comptaOM;
        private String baseCompta;

        public DossierCompta(String pbaseCompta, BSCPTAApplication100c pcomptaOM)
        {
            comptaOM = pcomptaOM;
            baseCompta = pbaseCompta;
        }

        public String _baseCompta { get { return baseCompta; } set { baseCompta = value; } }
        public BSCPTAApplication100c _comptaOM { get { return comptaOM; } set { comptaOM = value; } }

    }
}
