﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_Excel.Models
{
    public class Ecriture
    {
        private string cg_num;
        private DateTime date;
        private string libelle;
        private double debit;
        private double credit;
        private string departement;
        private string centrecout;
        private string objectif;
        private double montanttva; 
        private double tauxtva;
        private string codeTaxe;
        private double montantHT;        
        private string piece;
        private string codeTaxeSage;
        private string compteTiers;
        private DateTime dateEcheance;


        public string Cg_num { get => cg_num; set => cg_num = value; }
        public DateTime Date { get => date; set => date = value; }
        public string Libelle { get => libelle; set => libelle = value; }
        public double Debit { get => debit; set => debit = value; }
        public double Credit { get => credit; set => credit = value; }
        public string Departement { get => departement; set => departement = value; }
        public string Centrecout { get => centrecout; set => centrecout = value; }
        public string Objectif { get => objectif; set => objectif = value; }
        public double Montanttva { get => montanttva; set => montanttva = value; }
        public double Tauxtva { get => tauxtva; set => tauxtva = value; }
        public string CodeTaxe { get => codeTaxe; set => codeTaxe = value; }
        public double MontantHT { get => montantHT; set => montantHT = value; }        
        public string Piece { get => piece; set => piece = value; }
        public string CodeTaxeSage { get => codeTaxeSage; set => codeTaxeSage = value; }
        public string CompteTiers { get => compteTiers; set => compteTiers = value; }
        public DateTime DateEcheance { get => dateEcheance; set => dateEcheance = value; }
    }
}
