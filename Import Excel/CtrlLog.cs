﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Import_Excel
{
    public static class CtrlLog
    {
        private static string separateur = "-----------------------------------------------------------";
        public static string filepath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\logs\\log_importEcritures_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
        public static string filelog = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\logs\\log_FichierImport.txt";

        /// <summary>
        /// Ecrit la première partie du log
        /// </summary>
        public static void DemarreLog()
        {
            if (!Directory.Exists(Path.GetDirectoryName(filepath)))
                Directory.CreateDirectory(Path.GetDirectoryName(filepath));
           

            StreamWriter wr = new StreamWriter(filepath, true);
            wr.WriteLine(separateur);
            wr.WriteLine("", true);
            wr.WriteLine(DateTime.Now.ToString() + " Démarrage de l'application d'import.", true);
            wr.Close();
        }

        /// <summary>
        /// Méthode servant à écrire un message dans le log
        /// </summary>
        /// <param name="messageLog"></param>
        public static void EcritLog(string messageLog)
        {
            StreamWriter wr = new StreamWriter(filepath, true);
            //wr.WriteLine(separateur);
            wr.WriteLine(DateTime.Now.ToString() + " : " + messageLog);
            //wr.WriteLine();
            wr.Close();
        }


        /// <summary>
        /// Ecrit à la fin du log
        /// </summary>
        public static void FinLog()
        {
            StreamWriter wr = new StreamWriter(filepath, true);
            wr.WriteLine(separateur);
            wr.WriteLine(DateTime.Now.ToString() + " :");
            wr.WriteLine("Fin du processus.");
            wr.WriteLine(separateur);
            wr.WriteLine(separateur);
            wr.Close();
        }

        #region
        /// <summary>
        /// Ecriture d'un fichier de log d'analyse par fichier
        /// </summary>
        public static void DemarreLogFichier()
        {
            if (!Directory.Exists(Path.GetDirectoryName(filepath)))
                Directory.CreateDirectory(Path.GetDirectoryName(filepath));

            StreamWriter wr = new StreamWriter(filelog, false);
            wr.WriteLine(separateur);
            wr.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": Démarrage de l'analyse de fichier.");
            wr.WriteLine(separateur);
            wr.Close();
        }

        /// <summary>
        /// Ecrit le résultat de l'analyse d'un fichier a importer dans le log en cas d'erreur.
        /// </summary>
        /// <param name="_msg"></param>
        public static void EcritLogFichier(string _msg)
        {
            StreamWriter wr = new StreamWriter(filelog, true);
            //wr.WriteLine(separateur);
            wr.WriteLine(_msg);
            //wr.WriteLine();
            wr.Close();
        }

        /// <summary>
        /// Cloture le log d'analyse de fichier a importer
        /// </summary>
        public static void FinLogFichier()
        {
            StreamWriter wr = new StreamWriter(filepath, true);
            wr.WriteLine(separateur);
            wr.WriteLine(DateTime.Now.ToString() + "Fin du de l'analyse du fichier.");
            wr.WriteLine(separateur);
            wr.WriteLine(separateur);
            wr.Close();
        }
        #endregion
    }
}
