﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using Import_Excel.Models;
using System.IO;

namespace Import_Excel
{
    public static class CtrlDataSQL
    {        
        // Vérifie la présence de la base historique sinon la créé
        public static void CreateHistoryBase()
        {
            try
            {
                using (SqlConnection cnx = new SqlConnection())
                {
                    SqlConnectionStringBuilder cnxstringbuilder = new SqlConnectionStringBuilder();
                    cnxstringbuilder["Data Source"] = ConfigurationManager.AppSettings["Serveur"];
                    cnxstringbuilder["Initial Catalog"] = "master";
                    cnxstringbuilder["integrated Security"] = true;
                    cnx.ConnectionString = cnxstringbuilder.ConnectionString;
                    cnx.Open();
                    string req = "if not exists(select * from sys.databases where name = 'BIGSI_IMPORT_ECRITURES') "
                        + "create database BIGSI_IMPORT_ECRITURES ";
                        
                    SqlCommand cmd = new SqlCommand(req, cnx);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "use BIGSI_IMPORT_ECRITURES if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TIGISI_FichiersImportes') "
                        + "Create table TIGISI_FichiersImportes( fichier_id int not null identity, nomfichier nvarchar(max), datefichier datetime, dossier nvarchar(max) )";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "use BIGSI_IMPORT_ECRITURES if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TIGSI_Ecritures') "
                        + "create table TIGSI_Ecritures (ecriture_id int not null identity, journal nvarchar(max), intitule nvarchar(max), dateecriture datetime, montant numeric(24, 6), sens nvarchar(1), dossier nvarchar(max), compteg nvarchar(13),  piece nvarchar(max) )";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erreur lors de la vérification de la base historique: " + e.Message);
            }
        }

        /// <summary>
        /// Insère le fichier importé dans l'historique des fichiers
        /// </summary>
        /// <param name="fichier"></param>
        /// <param name="_now"></param>
        /// <param name="_dossier"></param>
        public static void InsereFichierhisto(string _fichier, DateTime _now, string _dossier)
        {
            try
            {
                using (SqlConnection cnx = new SqlConnection())
                {
                    SqlConnectionStringBuilder cnxstringbuilder = new SqlConnectionStringBuilder();
                    cnxstringbuilder["Data Source"] = ConfigurationManager.AppSettings["Serveur"];
                    cnxstringbuilder["Initial Catalog"] = "BIGSI_IMPORT_ECRITURES";
                    cnxstringbuilder["integrated Security"] = true;
                    cnx.ConnectionString = cnxstringbuilder.ConnectionString;
                    cnx.Open();
                    string req = $"insert into TIGISI_FichiersImportes (nomfichier, datefichier, dossier) values ('{_fichier}', '{_now.ToString()}', '{_dossier}')";
                    SqlCommand cmd = new SqlCommand(req, cnx);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception eInsert)
            {
                CtrlLog.EcritLog("L'insertion du fichier " + _fichier + " dans la base historique a échoué. Message: " + eInsert.Message);
            }
        }

        /// <summary>
        /// Vérifie si un fichier a déjà été importé pour un dossier
        /// </summary>
        /// <param name="_fichier"></param>
        /// <param name="_dossier"></param>
        /// <returns></returns>
        public static bool VerifFichierDejaIntegre(string _fichier, string _dossier)
        {
            try
            {
                using (SqlConnection cnx = new SqlConnection())
                {
                    SqlConnectionStringBuilder cnxstringbuilder = new SqlConnectionStringBuilder();
                    cnxstringbuilder["Data Source"] = ConfigurationManager.AppSettings["Serveur"];
                    cnxstringbuilder["Initial Catalog"] = "BIGSI_IMPORT_ECRITURES";
                    cnxstringbuilder["integrated Security"] = true;
                    cnx.ConnectionString = cnxstringbuilder.ConnectionString;
                    cnx.Open();
                    string req = $"select * from TIGISI_FichiersImportes where nomfichier = '{_fichier}' and dossier = '{_dossier}'";
                    SqlCommand cmd = new SqlCommand(req, cnx);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    return rdr.HasRows;
                }

            }
            catch(Exception eF)
            {
                throw new Exception("Erreur lors de la vérification du fichier déjà importé: " + eF.Message);
            }
        }

        /// <summary>
        /// Insère une ligne d'écriture dans l'historique
        /// </summary>
        /// <param name="ecriture"></param>
        public static void InsertLigne(Ecriture ecriture, string _dossier)
        {
            try
            {
                using (SqlConnection cnx = new SqlConnection())
                {
                    SqlConnectionStringBuilder cnxstringbuilder = new SqlConnectionStringBuilder();
                    cnxstringbuilder["Data Source"] = ConfigurationManager.AppSettings["Serveur"];
                    cnxstringbuilder["Initial Catalog"] = "BIGSI_IMPORT_ECRITURES";
                    cnxstringbuilder["integrated Security"] = true;
                    cnx.ConnectionString = cnxstringbuilder.ConnectionString;
                    cnx.Open();
                    double montant = ecriture.Debit > 0 ? ecriture.Debit : ecriture.Credit;
                    string sens = ecriture.Debit > 0 ? "D" : "C";
                    string req = "insert into TIGSI_Ecritures (journal, intitule, dateecriture, montant, sens, dossier, compteg, piece) values "
                        + $"('{ConfigurationManager.AppSettings["Journal"]}', '{ecriture.Libelle.Replace("'", "''")}', '{ecriture.Date.ToString()}', "
                        + $"'{montant.ToString().Replace(",", ".")}', '{sens}', '{_dossier}', '{ecriture.Cg_num}', '{ecriture.Piece}' )";
                    SqlCommand cmd = new SqlCommand(req, cnx);
                    cmd.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {
                CtrlLog.EcritLog("Erreur lors de l'insertion de la ligne d'écriture dans l'historique d'importation: " + e.Message);
            }
        }

        /// <summary>
        /// Vérifie si une ligne a déjà été insérée ou non
        /// </summary>
        /// <param name="ecriture"></param>
        /// <param name="_dossier"></param>
        internal static bool VerifLigne(Ecriture ecriture, string _dossier)
        {
            try
            {
                using (SqlConnection cnx = new SqlConnection())
                {
                    SqlConnectionStringBuilder cnxstringbuilder = new SqlConnectionStringBuilder();
                    cnxstringbuilder["Data Source"] = ConfigurationManager.AppSettings["Serveur"];
                    cnxstringbuilder["Initial Catalog"] = "BIGSI_IMPORT_ECRITURES";
                    cnxstringbuilder["integrated Security"] = true;
                    cnx.ConnectionString = cnxstringbuilder.ConnectionString;
                    cnx.Open();
                    double montant = ecriture.Debit > 0 ? ecriture.Debit : ecriture.Credit;
                    string sens = ecriture.Debit > 0 ? "D" : "C";
                    string req = $"select * from TIGSI_Ecritures where dossier = '{_dossier}' and dateecriture = '{ecriture.Date.ToString()}' and montant = {montant.ToString().Replace(',', '.')} "
                        + $"and sens = '{sens}' and intitule = '{ecriture.Libelle.Replace("'", "''")}' and compteg = '{ecriture.Cg_num}' and piece = '{ecriture.Piece}'";
                    SqlCommand cmd = new SqlCommand(req, cnx);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    return rdr.HasRows;
                }
            }
            catch(Exception e)
            {
                throw new Exception("Erreur lors de la vérification de ligne déjà enregistrée. Message: " + e.Message);
            }
        }
        
    }



}
