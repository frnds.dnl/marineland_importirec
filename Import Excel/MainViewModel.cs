﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Collections.ObjectModel;
using Import_Excel.Models;
using Microsoft.Win32;
using System.IO;
using Objets100cLib;

namespace Import_Excel
{
    public class MainViewModel : DependencyObject, INotifyPropertyChanged
    {

        #region constructeur
        public MainViewModel(string _user, string _mdp, string _chemin)
        {
            try
            {
                user = _user;
                mdp = _mdp;
                chemin = _chemin;
                CtrlFile.recupCodeTaxe();
                GetDossier();
                CtrlOM.ouvertureBaseCompta(user, mdp, chemin);
                CtrlOM.DossierCompta = Path.GetFileNameWithoutExtension(chemin);
                CtrlDataSQL.CreateHistoryBase();
            }
            catch (Exception eCodesTaxe)
            {
                MessageBox.Show(eCodesTaxe.Message);
                this.Close(null);
            }

            // relay command event handler
            GetFile = new RelayCommand(SelectExcelFile);
            CloseApplication = new RelayCommand(Close);
            ImportFile = new RelayCommand(Import);
            OpenLog = new RelayCommand(OpenFileLog);
            ecritures = ListEcritures.ecritures;
        }
        #endregion

        #region properties and dependencies
        private string user;
        private string mdp;
        private string chemin;
        public bool enabledCheck = false;
        public bool workFailed = false;
        public bool workDone = false;
        private string failedText = string.Empty;
        private int lignesIntegrees;
        public string FailedText
        {
            get { return failedText; }
            set
            {
                failedText = value;
                OnPropertyChanged("FailedText");
                //SetValue(FailedTextProperty, value);
            }
        }

        public int LignesIntegrees
        {
            get { return lignesIntegrees; }
            set
            {
                LignesIntegrees = value;
                SetValue(LignesIntegreesProperty, value);
            }
        }

        private string dossierCompta;

        public string DossierCompta
        {
            get { return dossierCompta; }
            set
            {
                dossierCompta = value;
                SetValue(DossierComptaProperty, value);
            }
        }

        public bool EnabledCheck
        {
            get { return enabledCheck; }
            set
            {
                enabledCheck = value;
                SetValue(enabledCheckProperty, value);
            }
        }

        public bool WorkFailed
        {
            get { return workFailed; }
            set
            {
                workFailed = value;
                SetValue(WorKFailedProperty, value);
            }
        }

        public bool WorkDone
        {
            get { return workDone; }
            set
            {
                workDone = value;
                SetValue(WorkDoneProperty, value);
            }
        }

        public static readonly DependencyProperty LignesIntegreesProperty =
            DependencyProperty.Register("LignesIntegrees", typeof(int), typeof(MainViewModel), new UIPropertyMetadata(null));

        public static readonly DependencyProperty WorkDoneProperty =
            DependencyProperty.Register("WorKDone", typeof(bool), typeof(MainViewModel), new UIPropertyMetadata(null));

        public static readonly DependencyProperty WorKFailedProperty =
            DependencyProperty.Register("WorKFailed", typeof(bool), typeof(MainViewModel), new UIPropertyMetadata(null));

        public static readonly DependencyProperty enabledCheckProperty =
            DependencyProperty.Register("enabledCheck", typeof(bool), typeof(MainViewModel), new UIPropertyMetadata(null));

        public static readonly DependencyProperty DossierComptaProperty =
            DependencyProperty.Register("DossierComptaProperty", typeof(string), typeof(MainViewModel), new UIPropertyMetadata(null));

        private string selectedFile = string.Empty;

        public string SelectedFile
        {
            get { return selectedFile; }
            set
            {
                selectedFile = value;
                SetValue(selectedFileProperty, value);
            }
        }

        public static readonly DependencyProperty selectedFileProperty =
                DependencyProperty.Register("SelectedFile", typeof(string), typeof(MainViewModel), new UIPropertyMetadata(null));

        public ObservableCollection<Ecriture> ecritures
        {
            get { return (ObservableCollection<Ecriture>)GetValue(ecritureProperty); }
            set { SetValue(ecritureProperty, value); }
        }

        public static readonly DependencyProperty ecritureProperty =
            DependencyProperty.Register("ecriture", typeof(ObservableCollection<Ecriture>), typeof(MainViewModel), new UIPropertyMetadata(null));

        private string fileStatus = string.Empty;

        public string FileStatus
        {
            get { return fileStatus; }
            set
            {
                //fileStatus = value;
                SetValue(FileStatusProperty, value);
            }
        }

        public static readonly DependencyProperty FileStatusProperty =
            DependencyProperty.Register("FileStatus", typeof(string), typeof(MainViewModel), new PropertyMetadata(null));
        #endregion


        #region RelayCommands
        public RelayCommand GetFile { get; set; }
        public RelayCommand CloseApplication { get; set; }
        public RelayCommand ImportFile { get; set; }
        public RelayCommand OpenLog { get; set; }
        #endregion


        #region Methodes
        void SelectExcelFile(object parameter)
        {
            try
            {
                WorkDone = false;
                WorkFailed = false;
                FailedText = string.Empty;
                SelectedFile = string.Empty;
                FileStatus = string.Empty;
                ListEcritures.ecritures.Clear();
                ListEcritures.listeJournaux.Clear();
                lignesIntegrees = 0;
                // openfiledialog
                string filepath = "";
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.DefaultExt = ".csv";
                fileDialog.Filter = "Fichiers csv (*.csv)|*.csv";
                fileDialog.Multiselect = false;
                bool? result = fileDialog.ShowDialog();
                // récupère le fichier et en créé un temporaire
                if (result == true)
                {
                    filepath = fileDialog.FileName;
                    SelectedFile = filepath;
                    try
                    {
                        CtrlLog.DemarreLogFichier();
                        CtrlLog.EcritLogFichier("Décryptage et analyse du fichier " + Path.GetFileNameWithoutExtension(SelectedFile) + " avant import.");

                        if (!CtrlDataSQL.VerifFichierDejaIntegre(Path.GetFileName(SelectedFile), DossierCompta))
                        {

                            int fileExtract = CtrlFile.chargeFichier(filepath);
                            if (fileExtract == 0)
                            {
                                FileStatus = "Le fichier est intégrable";
                                EnabledCheck = true;
                            }
                            else
                            {
                                CtrlLog.EcritLogFichier("Le fichier est mal formé, impossible de l'intégrer.");
                                FileStatus = "Le fichier est mal formé, veuillez consulter le log.";
                            }
                        }
                        else
                        {
                            FileStatus = "Le fichier semble avoir déjà été intégré pour ce dossier.";
                            CtrlLog.EcritLogFichier("Le fichier semble avoir déjà été intégré pour ce dossier.");
                            
                        }
                        CtrlLog.FinLogFichier();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                }
            }
            catch (Exception eFile)
            {
                MessageBox.Show("Erreur lors de l'ouverture du fichier Excel : " + eFile.Message);
            }
        }

        void Close(object parameter)
        {
            Application.Current.Shutdown();
        }

        void Import(object parameter)
        {
            List<IBOEcriture3> listeEcritureLock = new List<IBOEcriture3>();
            try
            {
                System.Windows.Input.Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                double sumCredit = ecritures.Sum(c => Math.Round(c.Credit, 2));
                double sumDebit = ecritures.Sum(d => Math.Round(d.Debit, 2));

                List<double> listetest = ecritures.Select(e => e.Debit).ToList();

                if (Math.Round(ecritures.Sum(c => c.Credit), 2) != Math.Round(ecritures.Sum(d => d.Debit), 2))
                {
                    CtrlLog.EcritLogFichier("Le fichier n'est pas équilibré. Le montant des débits n'est pas égal au montant des crédits");
                    CtrlLog.EcritLog("Le fichier n'est pas équilibré. Le montant des débits n'est pas égal au montant des crédits");
                    WorkFailed = true;
                }



                try
                {
                    for (int m = 0; m < ListEcritures.listeJournaux.Count; m++)
                    {
                        Journal journAtester = ListEcritures.listeJournaux[m];
                        try
                        {
                            CtrlOM.verifJournaux(journAtester, listeEcritureLock);
                        }
                        catch (Exception ex)
                        {
                            CtrlLog.EcritLog("   Erreur : Dossier " + journAtester.dossierComptable + " ; Le Journal " + journAtester.codeJournal + " période " + journAtester.période.ToString("MM/yyyy") + " est déjà ouvert ou n'existe pas !");
                            CtrlLog.EcritLogFichier("   Erreur : Dossier " + journAtester.dossierComptable + " ; Le Journal " + journAtester.codeJournal + " période " + journAtester.période.ToString("MM/yyyy") + " est déjà ouvert ou n'existe pas !");
                            WorkFailed = true;
                        }
                    }
                }
                catch (Exception m)
                {
                    WorkFailed = true;
                    CtrlLog.DemarreLogFichier();
                    CtrlLog.EcritLogFichier(m.Message);
                    CtrlLog.FinLogFichier();
                    throw new Exception("Verouillage Journaux Comptables : " + m.Message);
                    
                }


                foreach (Ecriture ec in ListEcritures.ecritures)
                {
                    try
                    {
                        string Verifresult = CtrlOM.VerifEcriture(ec);
                        if (!string.IsNullOrEmpty(Verifresult))
                        {
                            CtrlLog.EcritLogFichier("La vérification de la ligne " + (ListEcritures.ecritures.IndexOf(ec) + 2).ToString() + " a échoué avec le/les message(s) suivant: " + Verifresult);
                            CtrlLog.EcritLog("La vérification de la ligne " + (ListEcritures.ecritures.IndexOf(ec) + 2).ToString() + " a échoué avec le/les message(s) suivant: " + Verifresult);
                            workFailed = true;
                        }
                    }
                    catch (Exception eEcr)
                    {
                        WorkFailed = true;
                        CtrlLog.EcritLog("Erreur lors de la vérification de la ligne n° " + (ecritures.IndexOf(ec) + 2).ToString() + ". Les objects métiers ont renvoyé le message suivant: " + eEcr);
                        CtrlLog.EcritLogFichier("Erreur lors de la vérification de la ligne n° " + (ecritures.IndexOf(ec) + 2).ToString() + ". Les objects métiers ont renvoyé le message suivant: " + eEcr);
                    }
                }

                if (WorkFailed) // si la vérification a échoué
                    FailedText = "La vérification avant écriture a relevé des erreurs potentielles. Veuillez consulter le log.";
                else // commence les écritures
                {
                    int[] integrationResult = CtrlOM.InscritEcritures(ListEcritures.ecritures);
                    if (integrationResult[0] == ListEcritures.ecritures.Count) // toutes les lignes ont été intégrées                    
                        FailedText = "L'intégration s'est déroulée avec succès.";
                    else if (integrationResult[1] != 0 && integrationResult[0] == 0 && integrationResult[2] == 0) // toutes les lignes ont déjà été intégrées                    
                        FailedText = "Le fichier a déjà été traité mais aucune écriture n'a été générée.";
                    else if (integrationResult[1] != 0 && integrationResult[0] != 0 && integrationResult[2] == 0)
                        FailedText = "Le traitement s'est déroulé avec succès. " + integrationResult[1].ToString() + " lignes ont été rejetéees car déjà intégrées";
                    else if (integrationResult[0] == 0 && integrationResult[1] != 0 && integrationResult[2] != 0)
                        FailedText = "Aucune ligne n'a été intégrée. " + integrationResult[1].ToString() + " lignes ont été rejetées car déjà intégrées. " + integrationResult[2].ToString()
                            + " lignes sont en erreur.";
                    else if (integrationResult[2] == ListEcritures.ecritures.Count)
                        FailedText = "Aucune ligne n'a été intégrée. " + integrationResult[2].ToString() + " lignes sont en erreur. Veuillez consulter le log.";

                    lignesIntegrees = integrationResult[0];

                    
                   

                    CtrlDataSQL.InsereFichierhisto(Path.GetFileName(SelectedFile), DateTime.Now, DossierCompta);
                    WorkDone = true;

                    CtrlLog.EcritLog("Opération d'insertion terminée: il y a eu " + integrationResult[0] + " lignes intégrées, " + integrationResult[1] + " lignes rejetées, "
                        + integrationResult[2] + " lignes en erreur.");
                    CtrlLog.EcritLogFichier("Opération d'insertion terminée: il y a eu " + integrationResult[0] + " lignes intégrées, " + integrationResult[1] + " lignes rejetées, "
                        + integrationResult[2] + " lignes en erreur.");
                    System.Windows.Input.Mouse.OverrideCursor = null;
                    // RAZ
                    SelectedFile = string.Empty;
                    FileStatus = string.Empty;
                    EnabledCheck = false;
                    ecritures.Clear();
                }
                CtrlLog.FinLogFichier();
                CtrlOM.removeEcritureLock(listeEcritureLock);
            }
            catch (Exception eImp)
            {
                CtrlOM.removeEcritureLock(listeEcritureLock);
                CtrlLog.EcritLog(eImp.Message);
                System.Windows.Input.Mouse.OverrideCursor = null;
                MessageBox.Show("Erreur: " + eImp.Message);
            }
        }

        void GetDossier()
        {
            DossierCompta = Path.GetFileName(chemin);
        }

        // Méthode pour ouvrir le fichier de log via hyperlien en cas d'erreur.
        void OpenFileLog(object parameter)
        {
            System.Diagnostics.Process.Start(CtrlLog.filepath);
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            try
            {
                CtrlOM.FermeCompta();
            }
            catch
            {
                // nothing to do here
            }
        }

        #endregion

        #region PropertyChanged EventHandler
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion
    }
}
