﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using Import_Excel.Models;
using Objets100cLib;

namespace Import_Excel
{
    public static class CtrlOM
    {
        public static BSCPTAApplication100c baseCompta;
        public static string CompteGenLock = ConfigurationManager.AppSettings["CGLock"];
        internal static string DossierCompta = "";

        #region ouverture fermeture sage
        public static void ouvertureBaseCompta(string user, string mdp, string chemincompta)
        {
            try
            {
                baseCompta = new BSCPTAApplication100c();
                baseCompta.Name = chemincompta;
                baseCompta.Loggable.UserName = user;
                baseCompta.Loggable.UserPwd = mdp;
                if (!baseCompta.IsOpen)
                {
                    baseCompta.Open();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur ouverture dossier Compta : " + ex.Message);
            }
        }

        public static void FermeCompta()
        {
            if (baseCompta != null && baseCompta.IsOpen)
            {
                baseCompta.Close();
            }
        }
        #endregion  

        /// <summary>
        /// Vérifie chaque écriture avant insertion du fichier
        /// </summary>
        /// <param name="ecritureATester"></param>
        /// <returns></returns>
        public static string VerifEcriture(Ecriture ecritureATester)
        {


            IBOEcriture3 bOEcriture3 = (IBOEcriture3)baseCompta.FactoryEcriture.Create();
            string errMsg = "";

            try
            {
                if (CtrlDataSQL.VerifLigne(ecritureATester, DossierCompta))
                    errMsg += $"Une ligne d'écriture Piece: {ecritureATester.Piece}, Intitule: {ecritureATester.Libelle}, Date: {ecritureATester.Date}, Code Comptable: {ecritureATester.Cg_num}, "
                        + $"MontantHT: {ecritureATester.MontantHT} semble avoir déjà été passée." + Environment.NewLine;
            }
            catch (Exception eVerif)
            {
                errMsg += $"Il y a eu une erreur lors de la vérification de l'exsitence de l'écriture Piece: {ecritureATester.Piece}, Intitule: {ecritureATester.Libelle}, Date: {ecritureATester.Date}, Code Comptable: {ecritureATester.Cg_num}, "
                         + $"MontantHT: {ecritureATester.MontantHT}. Message: " + eVerif.Message + Environment.NewLine;
            }



            try
            {
                bOEcriture3.Journal = baseCompta.FactoryJournal.ReadNumero(ConfigurationManager.AppSettings["Journal"]);
            }
            catch
            {
                errMsg += "Code journal inconnu: " + ConfigurationManager.AppSettings["Journal"] + Environment.NewLine; ;
            }
            try
            {
                bOEcriture3.CompteG = baseCompta.FactoryCompteG.ReadNumero(ecritureATester.Cg_num);
            }
            catch
            {
                errMsg += "Compte Général inconnu : " + ecritureATester.Cg_num + Environment.NewLine;
            }
            try
            {

                bOEcriture3.DateSaisie = ecritureATester.Date;
                bOEcriture3.EC_Intitule = ecritureATester.Libelle.Length > 35 ? ecritureATester.Libelle.Substring(0, 35) : ecritureATester.Libelle;
                bOEcriture3.EC_Montant = ecritureATester.Debit == 0 ? ecritureATester.Debit : ecritureATester.Credit;
                bOEcriture3.EC_Sens = ecritureATester.Debit == 0 ? EcritureSensType.EcritureSensTypeCredit : EcritureSensType.EcritureSensTypeDebit;
                bOEcriture3.EC_Piece = ecritureATester.Piece;

                // vérification des codes taxes
                if (!string.IsNullOrEmpty(ecritureATester.CodeTaxeSage))
                {
                    IBOTaxe3 taxe;
                    if (ecritureATester.Cg_num.StartsWith("6"))
                    {
                        string codetva = ecritureATester.CodeTaxeSage.Replace("C", "D");
                        taxe = baseCompta.FactoryTaxe.ReadCode(codetva);
                    }
                    else
                        taxe = baseCompta.FactoryTaxe.ReadCode(ecritureATester.CodeTaxeSage);
                    bOEcriture3.Taxe = taxe;
                    //IBOTaxe3 taxe = baseCompta.FactoryTaxe.ReadCode(ecritureATester.CodeTaxeSage);
                    //bOEcriture3.Taxe = taxe;
                }
            }
            catch (Exception eEcr)
            {
                errMsg += "La création de la ligne pour vérification a échoué." + eEcr.Message + Environment.NewLine;
            }

            #region Plan analytiques
            IBPAnalytique3 pAnalytiqueDepartement = null;
            IBPAnalytique3 pAnalytiqueCentreCout = null;
            IBPAnalytique3 pAnalytiqueObjectif = null;

            try
            {
                pAnalytiqueDepartement = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Département");
            }
            catch
            {
                errMsg += "Le plan analytique Département n'a pas pu être récupéré." + Environment.NewLine;
            }
            try
            {
                pAnalytiqueCentreCout = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Centre de coût");
            }
            catch
            {
                errMsg += "Le plan analytique Centre de coût n'a pas pu être récupéré." + Environment.NewLine;
            }
            try
            {
                pAnalytiqueObjectif = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Objectif");
            }
            catch
            {
                errMsg += "Le plan analytique Objectif n'a pas pu être récupéré" + Environment.NewLine;
            }
            #endregion


            try // plan analytique département
            {
                IBOCompteA3 cpteA;
                if (!string.IsNullOrEmpty(ecritureATester.Departement) && pAnalytiqueDepartement != null)
                {
                    cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueDepartement, ecritureATester.Departement);
                }
            }
            catch (Exception eDep)
            {
                errMsg += "La recherche du compte analytique " + ecritureATester.Departement + " pour le plan Département a échoué. Message: " + eDep.Message + Environment.NewLine;
            }
            try // plan analytique Centre cout
            {
                IBOCompteA3 cpteA;
                if (!string.IsNullOrEmpty(ecritureATester.Centrecout) && pAnalytiqueCentreCout != null)
                {
                    cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueCentreCout, ecritureATester.Centrecout);
                }
            }
            catch (Exception eCout)
            {
                errMsg += "La recherche du compte analytique " + ecritureATester.Centrecout + " pour le plan Centre de cout a échoué. Message: " + eCout.Message + Environment.NewLine;
            }
            try // plan analytique Objectif
            {
                IBOCompteA3 cpteA;
                if (!string.IsNullOrEmpty(ecritureATester.Objectif) && pAnalytiqueObjectif != null)
                {
                    cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueObjectif, ecritureATester.Objectif);
                }
            }
            catch (Exception eObj)
            {
                errMsg += "La recherche du compte analytique  " + ecritureATester.Objectif + " pour le plan objectif a échoué. Message: " + eObj.Message + Environment.NewLine;
            }


            // vérifie si le compte G autorise l'analytique et qu'il n'est pas du type bilan (4 ou 5)
            if (bOEcriture3.CompteG != null)
                if (!bOEcriture3.CompteG.CG_Num.StartsWith("4") && !bOEcriture3.CompteG.CG_Num.StartsWith("5"))
                {
                    if (!bOEcriture3.CompteG.CG_Analytique && (!string.IsNullOrEmpty(ecritureATester.Departement) || !string.IsNullOrEmpty(ecritureATester.Centrecout) || !string.IsNullOrEmpty(ecritureATester.Objectif)))
                    {
                        errMsg += "Le compte général " + bOEcriture3.CompteG.CG_Num + " n'autorise pas l'analytique.";
                    }
                }
            return errMsg;
        }

        public static int[] InscritEcritures(ObservableCollection<Ecriture> ecritures)
        {
            int[] result = new int[3];
            foreach (Ecriture ecriture in ecritures)
            {
                try
                {
                    bool ecritTVA = false;
                    IBOEcriture3 bOEcriture3 = (IBOEcriture3)baseCompta.FactoryEcriture.Create();
                    bOEcriture3.Journal = baseCompta.FactoryJournal.ReadNumero(ConfigurationManager.AppSettings["Journal"]);

                    bOEcriture3.CompteG = baseCompta.FactoryCompteG.ReadNumero(ecriture.Cg_num);
                    bOEcriture3.DateSaisie = ecriture.Date;
                    bOEcriture3.EC_Intitule = ecriture.Libelle.Length > 35 ? ecriture.Libelle.Substring(0, 35) : ecriture.Libelle;
                    if (ecriture.Debit != 0)
                    {
                        bOEcriture3.EC_Montant = ecriture.MontantHT != 0 ? ecriture.MontantHT : ecriture.Debit;
                        bOEcriture3.EC_Sens = EcritureSensType.EcritureSensTypeDebit;
                    }
                    else
                    {
                        bOEcriture3.EC_Sens = EcritureSensType.EcritureSensTypeCredit;
                        bOEcriture3.EC_Montant = ecriture.MontantHT != 0 ? ecriture.MontantHT : ecriture.Credit;
                    }

                    if (!string.IsNullOrEmpty(ecriture.CodeTaxe) && ecriture.CodeTaxe.ToUpper() != "EXO") // ecriture TVA si besoin
                    {
                        ecritTVA = true;
                    }

                    bOEcriture3.EC_Piece = ecriture.Piece;
                    bOEcriture3.Date = ecriture.Date;
                    // inscription des codes taxes
                    if (!string.IsNullOrEmpty(ecriture.CodeTaxeSage) && ecriture.CodeTaxe != "EXO")
                    {
                        IBOTaxe3 taxe;
                        if (ecriture.Cg_num.StartsWith("6"))
                        {
                            string codetva = ecriture.CodeTaxeSage.Replace("C", "D");
                            taxe = baseCompta.FactoryTaxe.ReadCode(codetva);
                        }
                        else
                            taxe = baseCompta.FactoryTaxe.ReadCode(ecriture.CodeTaxeSage);
                        bOEcriture3.Taxe = taxe;
                    }
                    // écriture de la ligne
                    bOEcriture3.WriteDefault();

                    #region Analytique
                    // si ce n'est pas un compte de bilan (4 ou 5)
                    if (!bOEcriture3.CompteG.CG_Num.StartsWith("4") && !bOEcriture3.CompteG.CG_Num.StartsWith("5"))
                    {

                        removeAnalytique(bOEcriture3);

                        if(bOEcriture3.CompteG.CG_Num == "758100")
                        {
                            CtrlLog.EcritLog("");
                        }

                        // gestion des analytiques
                        IBPAnalytique3 pAnalytiqueDepartement = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Département");
                        IBPAnalytique3 pAnalytiqueCentreCout = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Centre de coût");
                        IBPAnalytique3 pAnalytiqueObjectif = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Objectif");

                        // analytique département
                        if (!string.IsNullOrEmpty(ecriture.Departement))
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueDepartement, ecriture.Departement.Trim());
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = bOEcriture3.EC_Montant;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                        else // sinon section d'attente
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueDepartement, "ZZZ");
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = bOEcriture3.EC_Montant;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                        // analytique centre cout
                        if (!string.IsNullOrEmpty(ecriture.Centrecout))
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueCentreCout, ecriture.Centrecout.Trim());
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = bOEcriture3.EC_Montant;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                        else
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueCentreCout, "ZZZZZ");
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = bOEcriture3.EC_Montant;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                        // analytique objectif
                        if (!string.IsNullOrEmpty(ecriture.Objectif))
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueObjectif, ecriture.Objectif.Trim());
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = bOEcriture3.EC_Montant;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                        else
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueObjectif, "ZZZZZ");
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = bOEcriture3.EC_Montant;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                    }
                    #endregion

                    if (ecritTVA)
                    {
                        InscritTVA(ecriture);
                    }

                    result[0]++;
                    CtrlDataSQL.InsertLigne(ecriture, DossierCompta);


                }
                catch (Exception eLigne)
                {
                    CtrlLog.EcritLog("Erreur lors de l'écriture n° " + (ecritures.IndexOf(ecriture) + 1).ToString() + " avec le message suivant: " + eLigne.Message);
                    result[2]++;
                }
            }
            return result;
        }

        // écrit la TVA associée à une écriture de crédit (6, 7) si besoin
        private static void InscritTVA(Ecriture ecriture)
        {
            try
            {
                IBOEcriture3 bOEcriture3TVA = (IBOEcriture3)baseCompta.FactoryEcriture.Create();
                bOEcriture3TVA.Journal = baseCompta.FactoryJournal.ReadNumero(ConfigurationManager.AppSettings["Journal"]);
                if (!string.IsNullOrEmpty(ecriture.CodeTaxeSage))
                {
                    IBOTaxe3 taxe;
                    if (ecriture.Cg_num.StartsWith("6"))
                    {
                        string codetva = ecriture.CodeTaxeSage.Replace("C", "D");
                        taxe = baseCompta.FactoryTaxe.ReadCode(codetva);
                    }
                    else
                        taxe = baseCompta.FactoryTaxe.ReadCode(ecriture.CodeTaxeSage);
                    bOEcriture3TVA.Taxe = taxe;
                    bOEcriture3TVA.CompteG = taxe.CompteG;
                    bOEcriture3TVA.EC_Montant = ecriture.Montanttva;
                    bOEcriture3TVA.EC_Sens = ecriture.Debit == 0 ? EcritureSensType.EcritureSensTypeCredit : EcritureSensType.EcritureSensTypeDebit; ;
                    bOEcriture3TVA.DateSaisie = ecriture.Date;
                    bOEcriture3TVA.EC_Intitule = ecriture.Libelle.Length > 35 ? ecriture.Libelle.Substring(0, 35) : ecriture.Libelle;
                    bOEcriture3TVA.Date = ecriture.Date;
                    bOEcriture3TVA.EC_Piece = ecriture.Piece;
                    bOEcriture3TVA.WriteDefault();
                    // gestion analytique tva
                    IBOCompteG3 compteTVA = taxe.CompteG;
                    if (compteTVA.CG_Analytique)
                    {
                        IBPAnalytique3 pAnalytiqueDepartement = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Département");
                        IBPAnalytique3 pAnalytiqueCentreCout = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Centre de coût");
                        IBPAnalytique3 pAnalytiqueObjectif = (IBPAnalytique3)baseCompta.FactoryAnalytique.ReadIntitule("Objectif");
                        // analytique département
                        if (!string.IsNullOrEmpty(ecriture.Departement)) // 1ère section
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueDepartement, ecriture.Departement.Trim());
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3TVA.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = ecriture.Montanttva;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                        else // sinon section d'attente
                        {
                            IBOCompteA3 cpteA = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueDepartement, "ZZZZZ");
                            IBOEcritureA3 ecritureAnalytique = (IBOEcritureA3)bOEcriture3TVA.FactoryEcritureA.Create();
                            ecritureAnalytique.CompteA = cpteA;
                            ecritureAnalytique.EA_Montant = ecriture.Montanttva;
                            ecritureAnalytique.SetDefault();
                            ecritureAnalytique.WriteDefault();
                        }
                        // section d'attente pour plan analytique Objectif
                        IBOCompteA3 compteAObjectif = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueObjectif, "ZZZZZ");
                        IBOEcritureA3 analytiqueObjectif = (IBOEcritureA3)bOEcriture3TVA.FactoryEcritureA.Create();
                        analytiqueObjectif.CompteA = compteAObjectif;
                        analytiqueObjectif.EA_Montant = ecriture.Montanttva;
                        analytiqueObjectif.SetDefault();
                        analytiqueObjectif.WriteDefault();
                        // section d'attente pour plan analytique centre de cout
                        IBOCompteA3 compteACentreCout = baseCompta.FactoryCompteA.ReadNumero(pAnalytiqueCentreCout, "ZZZZZ");
                        IBOEcritureA3 analytiqueCC = (IBOEcritureA3)bOEcriture3TVA.FactoryEcritureA.Create();
                        analytiqueCC.CompteA = compteACentreCout;
                        analytiqueCC.EA_Montant = ecriture.Montanttva;
                        analytiqueCC.SetDefault();
                        analytiqueCC.WriteDefault();
                    }
                }
            }
            catch (Exception eTVA)
            {
                throw new Exception("Erreur lors de l'écriture de TVA: " + eTVA.Message);
            }
        }

        /// <summary>
        /// génère des écrittures de lock journaux
        /// </summary>
        /// <param name="journAtester"></param>
        /// <param name="listeEcritureAsupp"></param>
        internal static void verifJournaux(Journal journAtester, List<IBOEcriture3> listeEcritureAsupp)
        {
            IBOEcriture3 boEcriture3 = (IBOEcriture3)journAtester.dossierComptable._comptaOM.FactoryEcriture.Create();
            boEcriture3.Journal = journAtester.dossierComptable._comptaOM.FactoryJournal.ReadNumero(journAtester.codeJournal);
            boEcriture3.Journal.CouldModified();
            boEcriture3.EC_Montant = 1.0;
            boEcriture3.EC_Sens = EcritureSensType.EcritureSensTypeDebit;
            boEcriture3.Date = journAtester.période;
            boEcriture3.EC_Reference = "lock Journal";
            boEcriture3.EC_RefPiece = "lock Journal";
            boEcriture3.EC_Intitule = "lock Journal";

            boEcriture3.CompteG = journAtester.dossierComptable._comptaOM.FactoryCompteG.ReadNumero(CompteGenLock);
            try
            {
                boEcriture3.WriteDefault();
                boEcriture3.CouldModified();
                listeEcritureAsupp.Add(boEcriture3);
            }
            catch
            {
                throw new Exception("");
            }
        }

        /// <summary>
        /// Supprime les écritures de lock journaux
        /// </summary>
        /// <param name="listeEcritureASupp"></param>
        internal static void removeEcritureLock(List<IBOEcriture3> listeEcritureASupp)
        {
            foreach (IBOEcriture3 boEcriture3 in listeEcritureASupp)
            {
                boEcriture3.Remove();
            }
        }

        internal static void removeAnalytique(IBOEcriture3 ecriture3)
        {
            try
            {
                foreach(IBOEcritureA3 a3 in ecriture3.FactoryEcritureA.List)
                {
                    a3.Remove();
                }
            }
            catch(Exception eAnalytique)
            {
                throw eAnalytique;
            }
        }
    }
}
