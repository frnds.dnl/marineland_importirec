﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Import_Excel
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                MainViewModel viewModel;
                InitializeComponent();
#if DEBUG
                viewModel = new MainViewModel("<Administrateur>", "", @"C:\Users\Dfernandes\Documents\Marineland\MARINELAND.mae");
#else
                string[] args = Environment.GetCommandLineArgs();
                viewModel = new MainViewModel(args[1].ToString(), args[2].ToString(), args[3].ToString());
#endif
                Closing += viewModel.OnWindowClosing;
                DataContext = viewModel;

            }
            catch (Exception e)
            {
                MessageBox.Show("Erreur lors du démarrage de l'application: " + e.Message);
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Click sur le txt Status Fichier si non intégrable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (!txtFileStatus.Text.Contains("intégrable"))
                {
                    System.Diagnostics.Process.Start(CtrlLog.filelog);
                }
            }
            catch (Exception eFilelog)
            {
                MessageBox.Show("Erreur lors de l'ouverture du fichier de log de vérification du fichier: " + eFilelog.Message);
            }
        }

        /// <summary>
        /// Click sur le txt résultat test/intégration si erreur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtfailedText_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (!txtfailedText.Text.Contains("succès"))
                {
                    System.Diagnostics.Process.Start(CtrlLog.filelog);
                }
            }
            catch { } // rien ici
        }

        /// <summary>
        /// hover sur txt status fichier si non intégrable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtFileStatus_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                if (!txtFileStatus.Text.Contains("intégrable"))
                {
                    Mouse.OverrideCursor = Cursors.Hand;
                }
            }
            catch
            { // rien                
            }
        }

        /// <summary>
        /// Quitte Hover txt file status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtFileStatus_MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = null;
        }

        /// <summary>
        /// hover sur txt résultat test/intégration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtfailedText_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                if (!txtfailedText.Text.Contains("succès"))
                {
                    Mouse.OverrideCursor = Cursors.Hand;
                }
            }
            catch { } // rien ici
        }

        /// <summary>
        /// Quitte hover sur txt résultat test/intégration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtfailedText_MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = null;
        }
    }
}
